package se.arbetsformedlingen.iris.annonsvisning.stream;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.state.*;
import se.arbetsformedlingen.iris.annonsvisning.model.AnnonsvisningAccumulator;
import se.arbetsformedlingen.iris.annonsvisning.store.AnnonsvisningStore;
import se.arbetsformedlingen.iris.annonsvisning.transformer.AnnonsvisningTransformer;
import se.arbetsformedlingen.kafka.Annonsvisning;

import java.util.Map;
import java.util.Properties;


public class AnnonsvisningStream {

    private final String host = "omegateam.se";
    private final String topic = "test_annonsvisningar";

    //private final String host = "sauron.ws.ams.se";
    //private final String topic = "iris_platsannons_visning";


    private final String storeName = "annonsvisningStore";

    private AnnonsvisningStore annonsvisningStore;

    public AnnonsvisningStream() {
        KafkaStreams streams  = createStream(storeName);
        System.out.println("host: " + host);
        System.out.println(streams.allMetadata());

        annonsvisningStore = new AnnonsvisningStore(streams, storeName);
    }

    public AnnonsvisningStore getAnnonsvisningStore() {
        return annonsvisningStore;
    }

    public static void main(String[] args) throws Exception {

        AnnonsvisningStream annonsvisningStream = new AnnonsvisningStream();
        //AnnonsvisningStore annonsvisningStore = annonsvisningStream.getAnnonsvisningStore();

        //System.out.println(annonsvisningStore.getStore().get("10518497"));
    }

    public KafkaStreams createStream(String storeName) {

        String bootstap_servers = getHost(host, 19092);
        String registry_url = getProtocolHost(host, 8081);

        System.out.println("bootstap_servers:" + bootstap_servers);
        System.out.println("registry_url:" + registry_url);

        final StreamsBuilder builder = new StreamsBuilder();

        KStream<String, Annonsvisning> annonsvisningar = builder.stream(topic);

        //annonsvisningar.print(Printed.<String, Annonsvisning>toSysOut().withLabel("annonsvisning"));

        // adding State to processor
        StoreBuilder<KeyValueStore<String, Integer>> storeBuilder = getStoreBuilder(storeName);

        builder.addStateStore(storeBuilder);

        KStream<String, AnnonsvisningAccumulator> statefulAnnonsvisningAccumulator = annonsvisningar.transformValues(
                () ->  new AnnonsvisningTransformer(storeName), storeName);

        statefulAnnonsvisningAccumulator.print(Printed.<String, AnnonsvisningAccumulator>toSysOut().withLabel("annonsvisningar"));

        Properties streamsConfiguration = getStreamsConfiguration(bootstap_servers, registry_url);
        final KafkaStreams streams = new KafkaStreams(builder.build(), streamsConfiguration);

        streams.start();
        System.out.println("Stream started");

        // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));

        return streams;

    }


    private Properties getStreamsConfiguration(String bootstap_servers, String registry_url) {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "test_platsannons_visning");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstap_servers);
        streamsConfiguration.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, registry_url);

        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
       // streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);

        streamsConfiguration.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 1000);
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        return streamsConfiguration;
    }

    private StoreBuilder<KeyValueStore<String, Integer>> getStoreBuilder(String storeName) {
        KeyValueBytesStoreSupplier storeSupplier = Stores.inMemoryKeyValueStore(storeName);
        StoreBuilder<KeyValueStore<String, Integer>> storeBuilder = Stores.keyValueStoreBuilder(storeSupplier, Serdes.String(), Serdes.Integer());
        return storeBuilder;
    }

    private String getHost(String defaultHost, int port) {
        Map<String, String> env = System.getenv();
        return (env.get("HOST") != null ? env.get("HOST" + ":" + port) : defaultHost + ":" + port);
    }

    private String getProtocolHost(String defaultHost, int port) {
        Map<String, String> env = System.getenv();
        return (env.get("HOST") != null ? "http://" + env.get("HOST") + ":" + port : "http://"  + defaultHost + ":" + port);
    }


}
