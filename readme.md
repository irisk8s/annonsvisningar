# Statistik för annonsvisningar

This RESTful service provides click data analytics for ad views.

## Endpoints used by Kubernetes
- `/` - Test endpoint
- `/annonser/{id}/annonsvisningar`

This quickstart exposes the following endpoints important for Kubernetes deployments:
- `/actuator/health` - Spring Boot endpoint indicating health. Used by Kubernetes as readiness probe.
- `/actuator/metrics` - Prometheus metrics. Invoked periodically and collected by Prometheus Kubernetes scraper.
