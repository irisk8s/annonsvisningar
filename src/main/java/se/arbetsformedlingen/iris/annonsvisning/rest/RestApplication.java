package se.arbetsformedlingen.iris.annonsvisning.rest;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.arbetsformedlingen.iris.annonsvisning.model.Annonsvisningar;
import se.arbetsformedlingen.iris.annonsvisning.store.AnnonsvisningStore;
import se.arbetsformedlingen.iris.annonsvisning.stream.AnnonsvisningStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;

@SpringBootApplication
@Controller
public class RestApplication {

	@Autowired
	private MeterRegistry registry;

	private static AnnonsvisningStore annonsvisningStore;

	@GetMapping(path = "/", produces = "application/json")
	@ResponseBody
	public Map<String, Object> landingPage() {
		Counter.builder("mymetric").tag("foo", "bar").register(registry).increment();
		return singletonMap("hello", "Jenkins X world with new feature 1");
	}


	@GetMapping(path = "/annonser/{annonsId}/annonsvisningar", produces = "application/json")
	@ResponseBody
	public Annonsvisningar getAnnonsvisningar(@PathVariable("annonsId") String annonsId) {
		Integer antal = annonsvisningStore.getStore().get(annonsId);
		return new Annonsvisningar(annonsId, antal);
	}

	@GetMapping(path = "/annonser/annonsvisningar", produces = "application/json")
	@ResponseBody
	public List<Annonsvisningar> getAnnonsvisningarList(@RequestParam(name="annonsIdn", required=true) String annonsIdn) {
		List<String> annonsIdList = Arrays.asList(annonsIdn.split("\\s*,\\s*"));

		List<Annonsvisningar> annonsvisningarList = new ArrayList<Annonsvisningar>();

		for (String annonsId : annonsIdList) {
			Integer antal = annonsvisningStore.getStore().get(annonsId);
			annonsvisningarList.add(new Annonsvisningar(annonsId, antal));
		}
		return annonsvisningarList;
	}

	public static void main(String[] args) {
        AnnonsvisningStream annonsvisningStream = new AnnonsvisningStream();
        annonsvisningStore = annonsvisningStream.getAnnonsvisningStore();

		SpringApplication.run(RestApplication.class, args);
	}

}
