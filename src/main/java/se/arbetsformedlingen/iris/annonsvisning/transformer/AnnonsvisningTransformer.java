package se.arbetsformedlingen.iris.annonsvisning.transformer;

import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import se.arbetsformedlingen.iris.annonsvisning.model.AnnonsvisningAccumulator;
import se.arbetsformedlingen.kafka.Annonsvisning;

import java.util.Objects;


public class AnnonsvisningTransformer implements ValueTransformer<Annonsvisning, AnnonsvisningAccumulator> {

    private KeyValueStore<String, Integer> stateStore;
    private final String storeName;
    private ProcessorContext context;

    public AnnonsvisningTransformer(String storeName) {
        Objects.requireNonNull(storeName,"Store Name can't be null");
        this.storeName = storeName;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
        stateStore = (KeyValueStore) this.context.getStateStore(storeName);
    }

    @Override
    public AnnonsvisningAccumulator transform(Annonsvisning value) {
        AnnonsvisningAccumulator annonsvisningAccumulator = AnnonsvisningAccumulator.builder(value).build();
        Integer accumulatedSoFar = stateStore.get(annonsvisningAccumulator.getAnnonsId());

        if (accumulatedSoFar != null) {
            annonsvisningAccumulator.addAnnonsvisningar(accumulatedSoFar);
        }
        stateStore.put(annonsvisningAccumulator.getAnnonsId(), annonsvisningAccumulator.getAntalVisningar());

        return annonsvisningAccumulator;

    }

    @Override
    public void close() {
        //no-op
    }
}
