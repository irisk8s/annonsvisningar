
package se.arbetsformedlingen.iris.annonsvisning.model;


import se.arbetsformedlingen.kafka.Annonsvisning;
import java.util.Objects;

public class AnnonsvisningAccumulator {

    private String annonsId;
    private int antalVisningar;

    public AnnonsvisningAccumulator(String annonsId, int antalVisningar) {
        this.annonsId = annonsId;
        this.antalVisningar = antalVisningar;
    }

    public String getAnnonsId() {
        return annonsId;
    }

    public int getAntalVisningar() {
        return antalVisningar;
    }

    public void addAnnonsvisningar(int tidigareAnnonsvisningar) {
        this.antalVisningar += tidigareAnnonsvisningar;
    }

    @Override
    public String toString() {
        return "annonsId='" + annonsId + ", antalVisningar=" + antalVisningar + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnnonsvisningAccumulator that = (AnnonsvisningAccumulator) o;
        return antalVisningar == that.antalVisningar &&
                Objects.equals(annonsId, that.annonsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(annonsId, antalVisningar);
    }

    public static Builder builder(Annonsvisning annonsvisning) {
        return new Builder(annonsvisning);
    }

    public static final class Builder {
        private String annonsId;
        private int antalVisningar;

        private Builder(Annonsvisning annonsvisning){
           this.annonsId = annonsvisning.getAnnonsId();
           this.antalVisningar = 1;
        }

        public AnnonsvisningAccumulator build(){
            return new AnnonsvisningAccumulator(annonsId, antalVisningar);
        }

    }
}
