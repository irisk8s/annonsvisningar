package se.arbetsformedlingen.iris.annonsvisning.store;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.errors.InvalidStateStoreException;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import se.arbetsformedlingen.kafka.Annonsvisning;

public class AnnonsvisningStore {

    private ReadOnlyKeyValueStore<String, Integer> store;
    private boolean initialized;


    public AnnonsvisningStore(KafkaStreams streams, String storeName) {
        this.store = initStore(streams, storeName);
    }

    public ReadOnlyKeyValueStore<String, Integer> getStore() {
        return store;
    }

    private ReadOnlyKeyValueStore<String, Integer> initStore(KafkaStreams streams, String storeName) {

        ReadOnlyKeyValueStore<String, Integer> store = null;

        System.out.print("Initializing StateStore ");

        while (!initialized) {

            try {
                store = streams.store(storeName, QueryableStoreTypes.keyValueStore());
                initialized = true;
            } catch (InvalidStateStoreException e) {
                System.out.print(".");
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(" done.");
        return store;

    }
}
