package se.arbetsformedlingen.iris.annonsvisning.model;

public class Annonsvisningar {

    private String annonsId;
    private Integer antal;

    public Annonsvisningar(String annonsId, Integer antal) {
        this.annonsId = annonsId;
        this.antal = antal;
    }

    public String getAnnonsId() {
        return annonsId;
    }

    public Integer getAntal() {
        return antal;
    }
}
